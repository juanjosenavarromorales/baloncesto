<%@ page import ="java.util.ArrayList"%>
<%@ page import ="java.util.List"%>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="es" xml:lang="es">

<head>
    <title>Votaci&oacute;n mejor jugador liga ACB</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
</head>

<body class="resultado">
    <span style='font-size: 10pt;'>
        Jugadores y sus votos
        <hr>
        <% 
        ArrayList<String> jugadores =new ArrayList<String>();
        jugadores = (ArrayList<String>) session.getAttribute("jugadores");
            for(String jugador : jugadores) {
                %> <span><% out.println(jugador); %></span>
                    <br>
                    <% } %>
    </span>
    <br> <a href="index.html"> Ir al comienzo</a>
</body>

</html>