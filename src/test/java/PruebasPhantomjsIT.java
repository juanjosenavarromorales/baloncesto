import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.openqa.selenium.By;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());

        driver.close();
        driver.quit();
    }

    @Test
    public void votacionesCeroTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        driver.findElement(By.name("reinicializarVotos")).click();
        driver.findElement(By.name("ver_votos")).click();
        assertEquals(driver.findElement(By.cssSelector("span:nth-child(2)")).getText(), "Carroll - 0");
        assertEquals(driver.findElement(By.cssSelector("span:nth-child(4)")).getText(), "Llull - 0");
        assertEquals(driver.findElement(By.cssSelector("span:nth-child(6)")).getText(), "Reyes - 0");
        assertEquals(driver.findElement(By.cssSelector("span:nth-child(8)")).getText(), "Rudy - 0");

        driver.close();
        driver.quit();
    }

    @Test
    public void nuevoJugadorTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        driver.findElement(By.name("txtOtros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys("JuanJose");
        driver.findElement(By.cssSelector("p:nth-child(12) > input:nth-child(1)")).click();
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.name("ver_votos")).click();
        driver.findElement(By.cssSelector("html")).click();
        assertEquals(driver.findElement(By.cssSelector("span:nth-child(10)")).getText(), "JuanJose - 1");

        driver.close();
        driver.quit();
    }
    
}