import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ModeloDatosTest {

    // @Test
    // public void testExisteJugador() {
    //     System.out.println("Prueba de existeJugador");
    //     String nombre = "";
    //     ModeloDatos bd = new ModeloDatos();
    //     bd.abrirConexion();
    //     boolean expResult = false;
    //     boolean result = bd.existeJugador(nombre);
    //     bd.cerrarConexion();
    //     assertEquals(expResult, result);
    //     // fail("Fallo forzado.");
    // }

    @Test
    public void testActualizarJugador() {
        System.out.println("Prueba actualizar jugador");
        String nombre = "JuanJose";
        ModeloDatos bd = new ModeloDatos();
        bd.abrirConexion();
        bd.insertarJugadorMock(nombre);
        bd.actualizarJugadorMock(nombre);
        assertEquals("2", bd.votosJugadorMock(nombre));
        bd.cerrarConexion();
    }
}